package nl.thomas0tter.servermanager.commands;

import nl.thomas0tter.servermanager.Main;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import nl.thomas0tter.servermanager.events.BanEvent;
import nl.thomas0tter.servermanager.Type;

import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.logging.Logger;


public class Commandban implements CommandExecutor {

	@SuppressWarnings("unused")
	private Main plugin;

	public Commandban(Main plugin) {
		this.plugin = plugin; }
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (sender.hasPermission("servermanager.ban")) {		
			if (args.length == 0) {
					if (args[0].equalsIgnoreCase("ban")) {
                        sender.sendMessage(ChatColor.RED + "Please specify a playername!");
						return true;
					}
	        Player target = Bukkit.getServer().getPlayer(args[0]);
			if (target == null) {	
				        sender.sendMessage(ChatColor.RED + "Could not find the specific player, " + sender.getName() + "!");
						return true;
            }
            target.kickPlayer(ChatColor.RED + "You have been banned from the server by " + ChatColor.WHITE + sender.getName());
            target.setBanned(true);
            Bukkit.getServer().getPluginManager().callEvent(new BanEvent(target, Type.BAN));
            return true;
			}