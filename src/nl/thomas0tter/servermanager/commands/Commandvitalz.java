package nl.thomas0tter.servermanager.commands;

import nl.thomas0tter.servermanager.Main;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Commandvitalz implements CommandExecutor {

	private Main plugin;

	public Commandvitalz(Main plugin) {
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		if (sender instanceof Player) {
			sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.RED + "Vitalz" + ChatColor.DARK_GRAY + "]" + ChatColor.WHITE + " Version: "
					+ ChatColor.GOLD + plugin.getDescription().getVersion());
			sender.sendMessage("Commands to be displayed here!");
		}
		return true;
	}

}
