package nl.thomas0tter.servermanager.commands;

import nl.thomas0tter.servermanager.Main;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Commandping implements CommandExecutor {
	
	@SuppressWarnings("unused")
	private Main plugin;
	
	public Commandping(Main plugin) {
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			player.sendMessage("Pong. You like Ping-Pong too, " + sender.getName() + "?");
		} else {
			System.out.println("Pong. You like Ping-Pong too, " + sender.getName() + "?");
		}
		return true;
	}
}
