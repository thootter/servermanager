package nl.thomas0tter.servermanager.commands;

import java.util.List;

import nl.thomas0tter.servermanager.Main;
import nl.thomas0tter.servermanager.exceptions.*;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Commandclearinventory implements CommandExecutor {

	private Main plugin;

	public Commandclearinventory(Main plugin) {
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		Player player = (Player) sender;

		if (sender instanceof Player) {
			if (player.hasPermission("vitalz.ci")) {
				if (args.length > 0 && player.hasPermission("vitalz.ci.others")) {
					List<Player> targets = plugin.getServer().matchPlayer(
							args[0]);
					if (!targets.isEmpty()) {
						for (Player t : targets) {
							t.getInventory().clear();
							player.sendMessage(ChatColor.AQUA
									+ "Inventory of: " + t.getDisplayName()
									+ " has been cleared!");
						}
					} else {
						try {
							throw new PlayerNotFoundException(player);
						} catch (PlayerNotFoundException e) {
							e.printStackTrace();
						}
					}
				} else {
					player.getInventory().clear();
					player.sendMessage(ChatColor.AQUA
							+ "Your inventory has been cleared!");
				}
			}
		}
		return false;
	}
}
