package nl.thomas0tter.servermanager;

import java.util.List;
import java.util.logging.Logger;

import nl.thomas0tter.servermanager.commands.Commandbroadcast;
//import nl.thomas0tter.servermanager.commands.Commandclearinventory;
import nl.thomas0tter.servermanager.commands.Commandfly;
import nl.thomas0tter.servermanager.commands.Commandhelp;
import nl.thomas0tter.servermanager.commands.Commandpl;
import nl.thomas0tter.servermanager.commands.Commandtime;
import nl.thomas0tter.servermanager.commands.Commandvanish;
import nl.thomas0tter.servermanager.commands.Commandvitalz;
import nl.thomas0tter.servermanager.commands.Commandtop;
import nl.thomas0tter.servermanager.commands.Commandban;
import nl.thomas0tter.servermanager.commands.Commandping;
import nl.thomas0tter.servermanager.commands.Commandgamemode;
//import nl.thomas0tter.servermanager.commands.Commandtp;

import nl.thomas0tter.servermanager.listeners.PlayerJoinListener;

import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	public List<String> MOTD;

	@Override
	public void onEnable() {
		final PluginManager pm = this.getServer().getPluginManager();}

		public final Logger logger = Logger.getLogger("minecraft");
		
		  {
		   logger.info("############################");
		   logger.info("");
		   logger.info("");
		   logger.info("ServerManager is a project by Thootter");
		   logger.info("Powered by UnknownNetwork.net");
		   logger.info("");
		   logger.info("You are currently running version 1.0");
		   logger.info("");
		   logger.info("");
		   logger.info("############################");

		getCommand("pl").setExecutor(new Commandpl(this));
		getCommand("help").setExecutor(new Commandhelp(this));
		getCommand("vitalz").setExecutor(new Commandvitalz(this));
		getCommand("vanish").setExecutor(new Commandvanish(this));
		getCommand("fly").setExecutor(new Commandfly(this));
		getCommand("time").setExecutor(new Commandtime(this));
		getCommand("top").setExecutor(new Commandtop(this));
		getCommand("ping").setExecutor(new Commandping(this));
		getCommand("gamemode").setExecutor(new Commandgamemode(this));
		getCommand("gm").setExecutor(new Commandgamemode(this));
		getCommand("ban").setExecutor(new Commandban(this));
		//getCommand("tp").setExecutor(new Commandtp(this));
		getCommand("broadcast").setExecutor(new Commandbroadcast(this));
		//getCommand("clearinventory").setExecutor(new Commandclearinventory(this));

		System.out.println("[ServerManager] Commands registered!");

		pm.registerEvents(new PlayerJoinListener(this), this);

		this.saveDefaultConfig();
		if (!this.getConfig().getStringList("motd").isEmpty()) {
			MOTD = this.getConfig().getStringList("motd");
		}

		super.onEnable();
	}

	@Override
	public void onDisable() {
		System.out.println("[ServerManager] Version ["
				+ this.getDescription().getVersion() + "] is disabled.");
		super.onDisable();
	}
}