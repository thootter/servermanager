package nl.thomas0tter.servermanager.exceptions;

import org.bukkit.entity.Player;

public class NotEnoughArgumentsException extends Exception {

	public NotEnoughArgumentsException(Player player) {
		player.sendMessage("Not Enough Arguments Given!");
	}

}
